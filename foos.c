#include<stdio.h>

#define N 36


/*--- struct ---*/
typedef struct{
  
  int id;//チームの区別
  int name;//駒の名前//
  int move;//移動するかどうかの判定
  int attack;//攻撃するかどうかの判定
  int action;//行動できるかどうかの判定
  int hp;//ヒットポイント2まで
  int alive;//生きているかどうか
  
} Piece;

typedef struct{

  Piece x[36];
  
} Piece_Set;

typedef struct{

  Piece z;
  int ev;//駒があるかどうか

} Place;

typedef struct{

  //Piece_Set x,y;
  Place ban[20][20];//各場所にどの駒が置かれているかの情報

} Board;

typedef struct{

  int t;
  int y;

} Point;

/*--- prototypes ---*/
Piece_Set Init_Piece(int i);
Board Init_Board(Piece_Set ps1,Piece_Set ps2);
int Battle_P(Board b);
Board Input_Command(int i,Board b);
int Select_Piece(int i,Board b);
int Select_Action(void);
int Select_Tate(void);
int Select_Yoko(void);
int Select_Attack(void);
int Select_A_D(void);
Board Action(int i,Board b,int p_n,int p_m,int p_t,int p_y,int p_a,int p_d);
Board Attack_Result(Point e,Board b);
Point Search_B(int i,int n,Board b);
int Show_Board(Board b);
int Show_End(int win);
int B_End(Board b);
int All_Finish(int i,Board b);

/*--- main ---*/
int main(void){

  int win;
  Piece_Set ps1,ps2;
  Board b;//盤面の構造体

  ps1 = Init_Piece(1);
  ps2 = Init_Piece(2);
  
  b = Init_Board(ps1,ps2);//盤面の初期化

  Show_Board(b);//スタート画面の提示：ルール説明
  win = Battle_P(b);//対戦のプログラム：返すのは勝利したチーム
  Show_End(win);//終了画面:勝利チームの表示

  return 0;
}

/*--- functions ---*/
Piece_Set Init_Piece(int i){

  Piece_Set a;
  int l;
  
  if(i==1){
    for(l=0;l<N;l++){
      a.x[i].id   = 1;
      a.x[i].name = l+1;
      a.x[i].move = 0;
      a.x[i].attack = 0;
      a.x[i].action = 1;
      a.x[i].hp = 2;
      a.x[i].alive = 1;
    }
  }
  else if(i==2){
    for(l=0;l<N;l++){
      a.x[i].id   = 2;
      a.x[i].name = l+1;
      a.x[i].move = 0;
      a.x[i].attack = 0;
      a.x[i].action = 1;
      a.x[i].hp = 2;
      a.x[i].alive = 1;
    }
  }
return a;
}

Board Init_Board(Piece_Set ps1,Piece_Set ps2){

  //ps1が左下、ps2が右上
  Board b;
  int i,l;
  int j = 0;

  for(i=0;i<20;i++){
    if((0<=i)&&(i<=5)){
      for(l=0;l<14;l++){
	b.ban[i][l].ev = 0;//駒は存在していない
	b.ban[i][l].z.id = 0;
      }
      for(l=14;l<20;l++){
	b.ban[i][l].ev = 1;
	b.ban[i][l].z  = ps2.x[j];
      }
    }
    else if((14<=i)&&(i<=19)){
      for(l=6;l<20;l++){
	b.ban[i][l].ev = 0;//駒は存在していない
	b.ban[i][l].z.id = 0;
      }
      for(l=0;l<6;l++){
	b.ban[i][l].ev = 1;
	b.ban[i][l].z  = ps1.x[j];
      }
    }
  } 
    
  return b;

}

int Battle_P(Board b){

  int res;//試合の結果
  
  while(1){//戦いが終わるまで続く

    b = Input_Command(1,b);//先攻のコマンドの入力
    b = Input_Command(2,b);//後攻のコマンドの入力

    res = B_End(b);//試合の終了判定
    
    if(res!=0){//どちらかの駒が全滅したら終了
      break;
    }
  }
  
    return res;
}

Board Input_Command(int i,Board b){

  int p_n,p_m,p_t,p_y,p_a,p_d;

  
  while(1){
    p_n = Select_Piece(i,b);//動かす駒の番号を選択0~35
    p_m = Select_Action();//動かすかどうか。０なら動かず1なら動かす
    if(p_m==1){
      p_t = Select_Tate();//動かす縦の方向
      p_y = Select_Yoko();//動かす横の方向
    }else{
      p_t = p_y = 0;
    }
    p_a = Select_Attack();//攻撃するかどうか。0なら攻撃しない。1ならする
    if(p_a==1){
      p_d = Select_A_D();
    }else{
      p_d = 0;
    }

    
    if(i==1){
      b = Action(1,b,p_n,p_m,p_t,p_y,p_a,p_d);
    }
    else if(i==2){
      b = Action(1,b,p_n,p_m,p_t,p_y,p_a,p_d);
    }

    if(All_Finish(i,b)==1){//全ての駒について行動したかどうかの判定
      break;
    }
  }
  return b;
  
}

int Select_Piece(int i,Board b){

  int j,k;
  int p_n;
  int alive[36];
  int l=0;

  for(j=0;j<20;j++){
    for(k=0;k<20;k++){
      if((b.ban[j][k].ev==1)&&(b.ban[j][k].z.id==i)&&(b.ban[j][k].z.alive==1)){
	alive[l]=b.ban[j][k].z.name;
	l++;
      }else{
	alive[l]=0;
	l++;
      }
    }
  }
  
  printf("動かす駒を選んでください\n");
  for(l=0;l<36;l++){
    if(alive[l]!=0){
      if(i==1){
	printf("x%d,",alive[l]);
      }else{
	printf("y%d,",alive[l]);
      }
    }
  }
  printf("\n");

  printf("駒の番号：");
  scanf("%d",&p_n);
  printf("\n");
  
  return p_n;
}

int Select_Action(void){

  int p_m;

  printf("駒を動かしますか?\n");
  scanf("%d",&p_m);
  printf("\n");

  return p_m;
  
}

int Select_Tate(void){

  int p_t;

  printf("駒を縦に何マス動かしますか?\n");
  scanf("%d",&p_t);
  printf("\n");

  return p_t;
  
}

int Select_Yoko(void){

  int p_y;

  printf("駒を横に何マス動かしますか?\n");
  scanf("%d",&p_y);
  printf("\n");

  return p_y;
  
}

int Select_Attack(void){

  int p_a;

  printf("駒を攻撃しますか?\n");
  scanf("%d",&p_a);
  printf("\n");

  return p_a;
  
}

int Select_A_D(void){

  int p_d;

  printf("駒でどの方向を攻撃しますか?1:前2:後ろ3:右4:左\n");
  scanf("%d",&p_d);
  printf("\n");

  return p_d;
  
}

Board Action(int i,Board b,int p_n,int p_m,int p_t,int p_y,int p_a,int p_d){//駒を実際に動かす
  Point g;//攻撃をする側の位置
  Point e;//攻撃を受ける側の位置
  int y,x;
  
  g = Search_B(i,p_n,b);

  if(p_m==1){
      g.t = g.t + p_t;
      g.y = g.y + p_y;
    }

    if(p_a==1){
      switch(p_d){
      case 1://上
	e.t = g.t + 1;
	b = Attack_Result(e,b);//攻撃を受けた側の処理
	break;
      case 2://下
	e.t = g.t - 1;
	b = Attack_Result(e,b);
	break;
      case 3://左
	e.y = g.y - 1;
	b = Attack_Result(e,b);
	break;
      case 4://右
	e.y = g.y + 1;
	b = Attack_result(e,b);
	break;
      default:break;
      }
    }
    
    y = g.t;
    x = g.y;
    
    b.ban[y][x].z.action = 0; 

    return b;
}    

Board Attack_Result(Point e,Board b){//攻撃を受けた側の処理

  int a,b;
  int hp;

  a = e.t;
  b = e.y;
  
  hp = b.ban[a][b].z.hp - 1;

  if(hp==0){
    b.ban[a][b].z.alive = 0;
    b.ban[a][b].ev = 0;
  }
 
  return b;
  
}

Point Search_B(int i,int n,Board b){//駒のidとnameからその駒の位置を返す
  int i,j,k;
  Point p;

  for(j=0;j<20;j++){
    for(k=0;k<20;k++){
      if((b.ban[j][k].ev==1)&&(b.ban[j][k].z.id==i)&&(b.ban[j][k].z.alive==1)&&(b.ban[j][k].z.name==n)){
	p.y = j;
	p.t = k;
      } 
    }
  }

  return p;
}

int Show_Board(Board b){

  int j,k;

  for(j=0;j<20;j++){
    for(k=0;k<20;k++){
      if(b.ban[j][k].z.id == 1)
      printf("x%d\t",b.ban[j][k].z.id);
    }else if(b.ban[j][k].z.id ==2){
      printf("y%d\t",b.ban[j][k].z.id);
    }else{
      printf("0\t");
    }

    if(k==19){
      printf("\n");
    }
  }

  return 0;
}

int Show_End(int win){

  return 0;
}

int B_End(Board b){

  return 0;
}

int All_Finish(int i,Board b){

  return 0;
}
